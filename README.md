# PHP-Summarizer
Using php to summarize text


##How It Works

Here’s the summarization algorithm in a nutshell :

Split the input text into sentences, and split the sentences into words.
For each word, stem it and keep count of how many times it occurs in the text. Skip words that are very common, like “you”, “me”, “is” and so on (I just used the stopword list from the Open Text Summarizer).
Sort the words by their “popularity” in the text and keep only the Top 20 most popular words. The idea is that the most common words reflect the main topics of the input text. Also, the “top 20” threshold is a mostly arbitrary choice, so feel free to experiment with other numbers.
Rate each sentence by the words it contains. In this case I simply added together the popularity ratings of every “important” word in the sentence. For example, if the word “Linux” occurs 4 times overall, and the word “Windows” occurs 3 times, then the sentence “Windows bad, Linux – Linux good!” will get a rating of 11 (assuming “bad” and “good” didn’t make it into the Top 20 word list).
Take the X highest rated sentences. That’s the summary.

##Contents

- [x] `main.php` – a simple demo of the text summarizer script.
- [x] `includes/summarizer.php` – the Summarizer PHP class. Some of it is commented!
- [x] `includes/porter_stemmer.php`– a lousy stemmer script used by the summarizer class.
- [x] `includes/html_functions.php` – various utility functions (optional).

##Usage

`main.php` is an example of this software working, take `main.php` and convert it to what you want.

#####References

W-shadow.com, (2007). Simple Text Summarizer In PHP | W-Shadow.com. Available at: http://w-shadow.com/blog/2008/04/12/simple-text-summarizer-in-php/ 
